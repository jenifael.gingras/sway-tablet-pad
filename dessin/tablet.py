"""
Implement a simple daemon to translate drawing tablet pad input to keyboard combo.
"""
import logging
import logging as logger
import sys
import time
from pathlib import Path
from typing import Any, Dict, List

import uinput
from .libinput import LibInput, LibInputPath
from .libinput.constant import ButtonState, EventType
from .libinput.device import Device, DeviceCapability
from .libinput.event import TabletPadEvent
from pyudev import Context, Device as UDevice, Monitor, MonitorObserver
from uinput import ev


SUBSYSTEM = "hid"
DRIVER = "uclogic"
ADD_ACTION = "add"
REMOVE_ACTION = "remove"

SLEEP_TIME = 5

# Strip bug
# https://github.com/DIGImend/digimend-kernel-drivers/issues/521

# Let's add udev as a dep ...
# https://wayland.freedesktop.org/libinput/doc/1.11.3/udev_config.html#udev_device_type
# https://pyudev.readthedocs.io/en/latest/api/pyudev.html

RING_PLUS = -1
RING_MINUS = -2
MAX_RING_POSITION = 270
REJECT_DELTA = 2e6  # 1e6 = 1 second

# globals
last_ring_event = 0
last_ring_event_time = 0


UINPUT_CONFIG_KAMVAS_PRO_13 = {
    0: [uinput.KEY_LEFTCTRL, uinput.KEY_EQUAL],
    1: [uinput.KEY_LEFTCTRL, uinput.KEY_MINUS],
    2: [uinput.KEY_E],
    3: [uinput.KEY_LEFTCTRL, uinput.KEY_LEFTSHIFT, uinput.KEY_Z],
    4: [uinput.KEY_LEFTCTRL, uinput.KEY_Z],
    RING_PLUS: [uinput.KEY_RIGHTBRACE],
    RING_MINUS: [uinput.KEY_LEFTBRACE],
}
UINPUT_CONFIG_610X = {
    0: [uinput.KEY_LEFTCTRL, uinput.KEY_EQUAL],
    1: [uinput.KEY_LEFTCTRL, uinput.KEY_MINUS],
    3: [uinput.KEY_RIGHTBRACE],
    4: [uinput.KEY_LEFTBRACE],
    5: [uinput.KEY_E],
    6: [uinput.KEY_LEFTCTRL, uinput.KEY_LEFTSHIFT, uinput.KEY_Z],
    7: [uinput.KEY_LEFTCTRL, uinput.KEY_Z],
}


PRODUCT_ID_TO_CONFIG = {0x006D: UINPUT_CONFIG_610X, 0x006E: UINPUT_CONFIG_KAMVAS_PRO_13}


def create_uinput_device() -> Dict[str, Any]:
    """"""
    keys_ev = []
    for attr, val in ev.__dict__.items():
        if str(attr).startswith("KEY"):
            keys_ev.append(val)

    return uinput.Device(keys_ev)


def emit(device: uinput.Device, button: int, product_id: int) -> None:
    """
    Convert an input to a virtual keyboard output.
    A config is read using the product_id of the event.

    This is in theory allows the function to operate for multiple tablet at the same time.
    """
    config = PRODUCT_ID_TO_CONFIG.get(product_id)
    if config is None:
        logging.error("No config found for product id: %s", hex(product_id))
        return

    combo_to_emit = config.get(button)
    if combo_to_emit is None:
        logging.warning("Nothing to emit!")
        return

    repeat = 1
    if (
        combo_to_emit[0] == uinput.KEY_LEFTBRACE
        or combo_to_emit[0] == uinput.KEY_RIGHTBRACE
    ):
        repeat = 5

    for _ in range(0, repeat):
        logging.info("Emit: %s", combo_to_emit)
        device.emit_combo(combo_to_emit)


def emit_from_ring(
    device: uinput.Device, event: TabletPadEvent, product_id: int
) -> None:
    """
    The ring position max out at 270 degree.
    This is converted in a discrete virtual keyboard output using the appropriate configuration.
    """
    global last_ring_event, last_ring_event_time

    # Refactor extract, duplicated logic
    config = PRODUCT_ID_TO_CONFIG.get(product_id)
    if config is None:
        logging.error("No config for %s", product_id)
        return

    # Extract ring event
    ring_position = event.ring_position
    if ring_position == -1:
        last_ring_event_time = 0
        logging.debug("Finger lifted from ring")
        return

    absolute_ring_position = convert_to_position(ring_position)

    # Can we make it intelligent?
    event_time = event.time
    delta = event_time - last_ring_event_time

    # Get ring event from delta, if positive we are going up
    ring_event = RING_PLUS
    if absolute_ring_position - last_ring_event < 0:
        ring_event = RING_MINUS

    last_ring_event = absolute_ring_position
    last_ring_event_time = event.time
    if delta > REJECT_DELTA:
        logging.debug("Skipping first event, last event is stale.")
        return

    # duplicated logic
    combo_to_emit = config.get(ring_event)
    if combo_to_emit is None:
        logging.warning("Nothing to emit.")

    repeat = 1
    if (
        combo_to_emit[0] == uinput.KEY_LEFTBRACE
        or combo_to_emit[0] == uinput.KEY_RIGHTBRACE
    ):
        repeat = 1

    for _ in range(0, repeat):
        logging.info("Emit: %s", combo_to_emit)
        device.emit_combo(combo_to_emit)


def detect_devices(li: LibInputPath) -> List[Device]:
    logging.info("Adding all devices, let see ...")
    devices = []

    context = Context()
    for udevice in context.list_devices(subsystem=SUBSYSTEM).match_attribute("driver", DRIVER):
        logger.debug("Found interesting udev device: %s", udevice)

        new_device = add_udevice(udevice, li)
        if new_device is None:
            continue

        devices += new_device

    return devices


def add_udevice(device: UDevice, li: LibInputPath) -> List[Device]:
    events_children: List[UDevice] = []
    for child in device.children:
        child: Device
        if "event" in child.sys_name:
            events_children.append(child)

    if not events_children:
        # No child of interest
        return []

    logger.info("New %s detected at %s", DRIVER, device.sys_path)
    devices = []
    for child in events_children:
        path = Path(f"/dev/input/{child.sys_name}")
        logger.info("Adding dev: %s", path.as_posix())
        device = add_device(li, path)
        if device is None:
            continue

        devices.append(device)
    return devices


def add_device(li: LibInputPath, path: Path):
    """
    Take a pathlib Path and handle the device adding logic.
    Return a device or None.
    """
    logging.debug("Adding device at path: %s", path.as_posix())
    device: Device = li.add_device(path.as_posix())

    if device is None:
        logging.info("Removing bad device: %s", device.sysname)
        li.remove_device(device)
        return

    if DeviceCapability.TABLET_PAD not in device.capabilities:
        logging.info("Removing non tablet device: %s", device.sysname)
        li.remove_device(device)
        return

    return device


# Class: UdevMonitor

class UdevMonitor:
    """
    Handles udev monitoring.
    """
    def __init__(self, li, devices):
        self._libinput: LibInputPath = li
        self._devices: List[Device] = devices

    def remove_missing_devices(self):
        """
        On a remove action from udev monitoring we need to make sure
        """
        logger.info("Listing availables /dev/input/")
        availables_sys_names = []
        for path in Path("/dev/input/").iterdir():
            if not path.is_char_device():
                logging.debug("Skipping path: %s. Not a char device.", path.as_posix())
                continue

            availables_sys_names.append(path.name)

        self._libinput.suspend()
        removed_devices = []
        for dev in self._devices:
            dev: Device
            if dev.sysname in availables_sys_names:
                continue
            logger.info("Removing unavailable device: %s", dev.sysname)
            self._libinput.remove_device(dev)
            removed_devices.append(dev)

        for removed in removed_devices:
            self._devices.remove(removed)
        self._libinput.resume()

    def monitor(self):
        """
        When a tablet is added we should add it correctly.
        This setup a monitoring on udev for devices with the appropriate driver.
        """
        context = Context()
        monitor = Monitor.from_netlink(context)
        monitor.filter_by(subsystem=SUBSYSTEM)

        def log_event(action, device: UDevice) -> None:
            if not DRIVER != device.driver:
                # Not a device we are monitoring
                return

            if ADD_ACTION != action and REMOVE_ACTION != action:
                # Not interested in those events
                return

            if action == REMOVE_ACTION:
                self.remove_missing_devices()
                return

            # There is an interesting device
            time.sleep(SLEEP_TIME)
            self._libinput.suspend()
            self._devices += detect_devices(self._libinput)
            self._libinput.resume()

        observer = MonitorObserver(monitor, log_event)
        observer.start()


def print_info(devices: List[Device]):
    for device in devices:
        logging.info("Device information")
        logging.debug("Name: %s", device.name)
        logging.debug("Vendor id: %s", device.id_vendor)
        logging.debug("Product id: %s", device.id_product)
        logging.info("Sys name: %s", device.sysname)


def config_log(level):
    console_handler = logging.StreamHandler(sys.stdout)
    file_handler = logging.FileHandler("/tmp/tablet.log", mode="a")
    logging.basicConfig(
        level=level,
        format="{asctime} {levelname} {message}",
        style="{",
        handlers=[console_handler, file_handler],
    )


def convert_to_position(ring_position):
    """
    So far, the Kamvas Pro 13 strip is interpreted as a flattened ring.
    The ring position is given in degree, 180 being the lowest position.
    Zero is roughly at the middle.
    The top position is 270.

    This formula give us something more logical with zero at the bottom and 270 at the top.
    """
    return (180 - ring_position) % 360


class EventsRouter:
    """
    Receive and redispatch LibInput events.
    """
    def __init__(self, u_device, li: LibInputPath):
        self._uinput = u_device
        self._lib_input = li

    def receive(self, event):
        """
        Receive a LibInput event.
        """
        product_id = event.device.id_product
        logging.debug(event.type)
        logging.debug(product_id)
        if event.type == EventType.DEVICE_ADDED:
            return

        if event.type == EventType.TABLET_PAD_STRIP:
            logging.debug("Strip event")
            return

        if event.type == EventType.TABLET_PAD_RING:
            logging.debug("Ring Event")
            emit_from_ring(self._uinput, event, product_id)

        if (
            event.type == EventType.TABLET_PAD_BUTTON
            and event.button_state == ButtonState.PRESSED
        ):
            logging.debug(event.button_number)
            emit(self._uinput, event.button_number, product_id)
            return

    def listen(self):
        """
        Loop to receive libinput events.
        """
        for event in self._lib_input.events:
            self.receive(event)


def main(args):
    level = args.level
    config_log(level)

    logging.info("Init!")
    li: LibInputPath = LibInput()

    devices = detect_devices(li)
    print_info(devices)

    udev_monitor = UdevMonitor(li, devices)
    udev_monitor.monitor()

    logging.info("Init uinput dev")
    u_device = create_uinput_device()
    events_router = EventsRouter(u_device, li)
    logging.info("Wait for init uinput")
    time.sleep(SLEEP_TIME)

    logging.info("Ready: reading events")
    events_router.listen()  # main loop

    logging.info("Clean up.")
    u_device.destroy()
    for device in devices:
        li.remove_device(device)
    logging.info("END!")
