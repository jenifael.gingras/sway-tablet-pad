from pyudev import Context


HUION_ID = "256C"


# Get devices, filter huion ones, filter only those with actual name
context = Context()
huion = [d for d in context.list_devices() if HUION_ID in str(d)]
tablets = [d for d in huion if d.attributes.get('name') is not None]

# Print strip properties
strip = tablets[2]
print(strip)
for k in strip.properties.keys():
    print(k, ":", strip.properties.get(k))
