import time

from pyudev import Context, Device, Monitor, MonitorObserver

SUBSYSTEM = "hid"
DRIVER = "uclogic"
ACTION = "add"


context = Context()


for dev in context.list_devices(subsystem="hid").match_attribute("driver", "uclogic"):
    print(dev.sys_name)


monitor = Monitor.from_netlink(context)
monitor.filter_by(subsystem=SUBSYSTEM)


def log_event(action, device: Device) -> None:
    if DRIVER == device.driver and ACTION == action:
        print(device.sys_name)


observer = MonitorObserver(monitor, log_event)
observer.start()

while True:
    time.sleep(1)
