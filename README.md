# Sway tablet pad service

Drawing tablet like those of Wacom are not entirely supported in Sway.
The available button can not be used.
Tools like Krita do not allow to directly map those button to the tool actions.

This service is a workaround.
(The author forked the original lib as it is unmaintained and clearly untested for drawing tablets)
python-libinput is used to listen to the pad button events.
Then those presses are converted to a virtual keyboard combo through python-uinput.

The config is manageable in yaml files.


## Arch linux dependencies

- python-uinput
- python-pyudev


## Manual installation

A service file is provided.

1. Install the python dependencies: uinput and pyudev
2. Copy the module loading config: `cp uinput.conf /etc/modules-load.d/uinput.conf`
3. Copy the service file under: $HOME/.config/systemd/user
4. Add your user to groups plugdev and input: `usermod -aG input *user*` and `usermod -aG plugdev *user*`
5. Reboot (or at the very least close your session and login again)
6. `systemctl --user daemon-reload`
7. `systemctl --user enable tablet` (optional: to make it persistent through a reboot)
8. `systemctl --user start tablet`
9. Log can be read either at /tmp/tablet.log or `journalctl --user -u tablet`



## TODO

- [x]: Basic service
- []: Loading yaml config
- [x]: Bind a device through an id or a name instead of hardcoding to an /dev/input/eventX path. (udev based on uclogic driver)
- []: Document the thing
- []: Automated testing
- []: MAKEPKG example


## python-libinput fork

Why was the python-libinput project forked?
As mentionned in the original author git repository the project is unmaintained.
Supports for tablet pad events is completely broken with multiples import error.
When I have time I wish to bring back my modification to the upstream.
Until then I will use my own fork.

NB: the fork is vendorized under dessin/libinput


## udev

A udev file is supplied (udev.rules) to change some properties on the Huion tablets.


## LICENSE
GPLV3 (TODO: add the proper license file)
