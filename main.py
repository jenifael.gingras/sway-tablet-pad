"""
Entry point
"""
from argparse import ArgumentParser
import logging

from dessin import tablet


if __name__ == "__main__":
    parser = ArgumentParser(description="Make tablet great again")
    parser.add_argument(
        "--level",
        default=logging.INFO,
        help="Logger level (DEBUG=10, CRITICAL=50)",
        type=int,
    )

    args = parser.parse_args()
    tablet.main(args)
